FROM python:3.7-alpine

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

RUN pip install --upgrade pip

RUN source venv/bin/activate

RUN pip install -r requirements.txt

EXPOSE 5000
CMD ["python", "app.py"]